#include <iostream>
#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#ifdef WIN32
#include <windows.h>
#else
#include <termios.h>
#include <unistd.h>
#endif

#include <sstream>

using namespace std;

void SetStdinEcho(bool enable = true)
{
#ifdef WIN32
    HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
    DWORD mode;
    GetConsoleMode(hStdin, &mode);

    if( !enable )
        mode &= ~ENABLE_ECHO_INPUT;
    else
        mode |= ENABLE_ECHO_INPUT;

    SetConsoleMode(hStdin, mode );

#else
    struct termios tty;
    tcgetattr(STDIN_FILENO, &tty);
    if( !enable )
        tty.c_lflag &= ~ECHO;
    else
        tty.c_lflag |= ECHO;

    (void) tcsetattr(STDIN_FILENO, TCSANOW, &tty);
#endif
}

struct AccountData
{
    std::string user;
    std::string pass;
    std::string db;
    std::string port;
    std::string host;
    std::string table;
};

int main()
{
    AccountData data;
    cout << "Enter host (127.0.0.1 for localhost): ";
    cin >> data.host;
    cout << "Enter port (3306): ";
    cin >> data.port;
    cout << "Username: ";
    cin >> data.user;
    cout << "Password (won't be shown in the console): ";
    SetStdinEcho(false);
    cin >> data.pass;
    SetStdinEcho(true);
    cout << "\nDatabase (example: world): ";
    cin >> data.db;
    cout << "Table name (item_template): ";
    cin >> data.table;

    int y = 0;

    try {
        sql::Driver *driver;
        sql::Connection *con;
        sql::Statement *stmt;
        sql::ResultSet *res;
        driver = get_driver_instance();
        con = driver->connect("tcp://" + data.host + ":" + data.port, data.user, data.pass);
        con->setSchema(data.db);
        stmt = con->createStatement();
        res = stmt->executeQuery("SELECT entry, name, stat_type1, stat_type2, stat_type3, stat_type4, stat_type5, stat_type6, stat_type7, stat_type8, stat_type9, stat_type10, stat_value1, stat_value2, stat_value3, stat_value4, stat_value5, stat_value6, stat_value7, stat_value8, stat_value9, stat_value10 FROM " + data.table + " WHERE 3 in (stat_type1, stat_type2, stat_type3, stat_type4, stat_type5, stat_type6, stat_type7, stat_type8, stat_type9, stat_type10) AND 7 in (stat_type1, stat_type2, stat_type3, stat_type4, stat_type5, stat_type6, stat_type7, stat_type8, stat_type9, stat_type10)");
        while(res->next())
        {
            int s = 0;
            int a = 0;

            for (int i = 3; i < 13; ++i)
            {
                if (res->getUInt(i) == 3)
                    a = i - 2;
                if (res->getUInt(i) == 7)
                    s = i - 2;
            }

            if (s && a && res->getUInt(12 + s) != res->getUInt(12 + a))
            {
                std::ostringstream ss;
                ss << "UPDATE " << data.table << " SET stat_type" << a << "=stat_type" << s << " WHERE entry=" << res->getString("entry");
                stmt->executeUpdate(ss.str().c_str());
                ++y;
            }
        }
        cout << "Fixed: " << y << " items!\n";
        delete res;
        delete stmt;
        delete con;

  } catch (sql::SQLException &e) {
    cout << "# ERR: SQLException in " << __FILE__;
    cout << "(" << __FUNCTION__ << ") on line "
       << __LINE__ << endl;
    cout << "# ERR: " << e.what();
    cout << " (MySQL error code: " << e.getErrorCode();
    cout << ", SQLState: " << e.getSQLState() << " )" << endl;
  }

    return 0;
}
