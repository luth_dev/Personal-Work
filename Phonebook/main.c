#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
	char nume[31];
	char prenume[31];
	char telefon[11];
} Entry;

typedef struct
{
	Entry *data;
	int size;
	int size_max;
} Storage;

void InitStorage(Storage *s, int initSize);
void Insert(Storage *s, Entry e);
void Remove(Storage *s, int index);
void Edit(Storage *s, Entry e, int index);
Entry LineToEntry();
void LineToStore(Storage *s);
void FileToStore(Storage *s, FILE *f);
void StoreToFile(Storage *s, FILE *f,char *path);
void PrintStore(Storage *s);

int main()
{
	FILE *agenda = NULL;
	int x, y, n = 0;
	printf("1.Deschide o agenda\n2.Creeaza o noua agenda\nAlege: ");
	scanf("%i", &x);
	char *path = calloc(128, sizeof(char));
	printf("Locatie agenda: ");
	scanf("%100s", path);
	printf("\n");
	if (x == 1)
	{
		agenda = fopen(path, "r");
		if (agenda == NULL)
		{
			printf("Eroare: Agenda nu exista!\n");
			return 1;
		}
	}
	else if (x == 2)
	{
		agenda = fopen(path, "w+");
		if (agenda == NULL)
		{
			printf("Eroare: Nu s-a putut crea agenda!\n");
			return 1;
		}
	}
	else
	{
		printf("Eroare: Alegere invalida!\n");
		return 1;
	}
	Storage store;
	InitStorage(&store, 200);
	FileToStore(&store, agenda);
	PrintStore(&store);
	while(x != 5)
	{
		printf("1.Adauga o persoana\n2.Editeaza o persoana\n3.Sterge o persoana\n4.Afiseaza agenda\n5.Iesire\n\nAlege: ");
		scanf(" %i", &x);
		getc(stdin);
		switch(x)
		{
			case 1:
				LineToStore(&store);
				break;
			case 2:
				printf("ID persoana: ");
				scanf(" %i", &y);
				getchar();
				Edit(&store, LineToEntry(), y);
				break;
			case 3:
				printf("ID persoana: ");
				scanf("  %i", &y);
				Remove(&store, y);
				break;
			case 4:
				PrintStore(&store);
				break;
		}
	}
	StoreToFile(&store, agenda, path);
	fclose(agenda);
	printf("\n");
	return 0;
}

void InitStorage(Storage *s, int initSize)
{
	s->data = calloc(initSize, sizeof(Entry));
	s->size_max = initSize;
	s->size = 0;
}

void Insert(Storage *s, Entry e)
{
	s->data[s->size] = e;
	++(s->size);
}

void Remove(Storage *s, int index)
{
	if (index >= s->size)
	{
		printf("Eroare: Index > Storage size\n");
		return;
	}
	for (int i = index; i < s->size-1; ++i)
		s->data[i] = s->data[i+1];
	--(s->size);
}

void Edit(Storage *s, Entry e, int index)
{
	if (index >= s->size)
	{
		printf("Eroare: Index > Storage size\n");
		return;
	}
	s->data[index] = e;
}

Entry LineToEntry()
{
	Entry tmp;
	printf("Exemplu: Dumitru Ion 0712345678\n");
	char line[sizeof(Entry)];
	fgets(line, sizeof(line), stdin);
	if (sscanf(line, "%30s %30s %10s", tmp.nume, tmp.prenume, tmp.telefon) != 3)
	{
		printf("Eroare: Format invalid! %s\n", line);
		strcpy(tmp.nume, "NULL");
	}
	return tmp;
}

void LineToStore(Storage *s)
{
	Entry tmp = LineToEntry();
	if (strcmp(tmp.nume, "NULL") != 0)
		Insert(s, tmp);
}

void FileToStore(Storage *s, FILE *f)
{
	fseek(f, 0, SEEK_SET);
	Entry tmp;
	char line[sizeof(Entry)];
	while(fgets(line, sizeof(line), f))
		if (sscanf(line, "%30s %30s %10s", tmp.nume, tmp.prenume, tmp.telefon) == 3)
			Insert(s, tmp);
}

void StoreToFile(Storage *s, FILE *f,char *path)
{
	freopen(path, "w", f);
	for (int i = 0; i < s->size; ++i)
		fprintf(f, "%s %s %s\n", s->data[i].nume, s->data[i].prenume, s->data[i].telefon);
	freopen(path, "r", f);
}

void PrintStore(Storage *s)
{
	printf("Numar contacte: %i\n\n", s->size);
	for (int i = 0; i < s->size; ++i)
		printf("ID: %i \nNume: %s \nPrenume: %s \nTelefon: %s\n\n", i, s->data[i].nume, s->data[i].prenume, s->data[i].telefon);
}